/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijkstra;
/**
 *
 * @author ilya
 */
public class Sommet {
    Point p;
    Liste voisins;
    double dis;
    Sommet predecesseur;

    public Sommet(Point p) {
        this.p = p;
        this.voisins=new Liste();
        dis=0.0;
    }
    public Sommet(Sommet a){
        this.p=new Point(a.p);
        this.dis=a.dis;
        this.voisins=new Liste();
    }
    public Sommet getPred() {
        return predecesseur;
    }
    public void setPred(Sommet predecesseur) {
        this.predecesseur = predecesseur;
    }
    public Point getPoint(){
        return this.p;
    }
    public void setVoisin(Sommet p){
        voisins.add(p);
    }
    public void setDis(double dis) {
        this.dis = dis;
    }
    public double getDis() {
        return dis;
    }
    
    
}
