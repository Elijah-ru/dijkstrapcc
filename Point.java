/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijkstra;
/**
 *
 * @author im285503
 */
public class Point {
	int x;
	int y;
	
	public Point(int x, int y){
		this.x=x;
		this.y=y;
	}
	public Point(Point autre){
		this.x=autre.x;
		this.y=autre.y;
	}
	public Point(){
		this.x=0;
		this.y=0;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public void setX(int x) {
		this.x = x;
	}
	public void setY(int y) {
		this.y = y;
	}
	public double distance(Point p){
            return Math.sqrt(Math.pow((this.getX()-p.getX()), 2)+Math.pow((this.getY()-p.getY()), 2));         
        }
        public boolean equals(Point p){
            return (this.getX()==p.getX()&&this.getY()==p.getY());
        }
        /**
        *Renvoie true si le point est "en haut à droite" d'un autre passé en paramètre.
        * @param p point avec lequel comparer
        * @return boolean
        */
        public boolean estPlusGrandQue(Point p){
            return (p.getY()>=this.getY()&&p.getX()<=this.getX());
        }
        @Override
        public String toString() {
            return "P{"+x + ", " + y + '}';
        }
       
}
