/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijkstra;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;


/**
 *
 * @author ilya
 */
public class Jeu {
    Obstacle[] obstacles;
    Liste sommets, sauvsom; //listes de sommets uniquement pour le travail et pour garder les coordonnées des points
    Liste cheminf, sommetsaafficher; //liste de sommets a afficher
    
    //paramètres
    private final int[] taille;
    private final int shift=100;
    private final int pointsize=3;
    private final int rayraff=20;
    private int raygraph=100;
    private final int nbpts;
    
    public Jeu(int l, int h, int nbpts){
	obstacles=new Obstacle[4];
	taille=new int[2];
        this.nbpts=nbpts;
	sommets=new Liste();
        sauvsom=new Liste();
        cheminf=new Liste();
	sommetsaafficher=new Liste();
	taille[0]=l; taille[1]=h;
	}
    public void raffinement(){
            Liste chemin, somtemp;
            Point D=new Point(0, 600), A=new Point(800, 0);
            Sommet w;
            cheminf=new Liste();
            raygraph=30;
            int i=0;
            somtemp=sauvsom;
            do{//sens direct
                sauvsom=somtemp;
                genSommets(sauvsom);
                genGraph(A);
                sommetsaafficher=sommets.copie();
                chemin=PCC(D, A);
                i++;
                System.out.println(i);
                }while(chemin.trouve(new Sommet(A))<0);
            w=chemin.get(chemin.trouve(new Sommet(A)));
            while(!w.p.equals(D)){
                    System.out.println("pred");
                    cheminf.add(w);
                    w=w.getPred();
                }
            cheminf.add(chemin.get(0));
    }
    public void raffinement(int ecart){
            Liste chemin, grapheSD=null, grapheSI=null, somtemp;
            Point D=new Point(0, 600), A=new Point(800, 0);
            Sommet w;
            int i;
            boolean valide;
            if(ecart==Fenetre.rafmax){
                creerObstacles();
                do{
                    valide=true;
                    i=0;
                    do{//sens direct
                        genSommets();
                        genGraph(A);
                        sommetsaafficher=sommets.copie();
                        chemin=PCC(D, A);
                        i++;
                        }while(chemin.trouve(new Sommet(A))<0&&i<10);
                    System.out.println("SD  "+i);
                    if(i>=10||chemin.trouve(new Sommet(A))<0)
                        valide=false;
                    grapheSD=chemin;
                    if(valide){
                        i=0;
                        do{//sens inverse
                            sommets=sauvsom.copie();
                            genGraph(D);
                            chemin=PCC(A, D);
                            i++;
                            }while(chemin.trouve(new Sommet(D))<0&&i<10);
                        System.out.println("SI  "+i);
                        if(i>=10||chemin.trouve(new Sommet(D))<0)
                            valide=false;
                        grapheSI=chemin;
                    }
                }while(!valide);
            }
            else{
                System.out.println("Suivant");
                somtemp=sauvsom;
                raygraph=45;
                do{
                    valide=true;
                    i=0;
                    do{//sens direct
                        sauvsom=somtemp;
                        genSommets(sauvsom);
                        genGraph(A);
                        sommetsaafficher=sommets.copie();
                        chemin=PCC(D, A);
                        i++;
                        }while(chemin.trouve(new Sommet(A))<0&&i<10);
                    System.out.println("SD 2 "+i);
                    if(i>=10||chemin.trouve(new Sommet(A))<0){
                        valide=false;
                        sauvsom=somtemp;
                    }
                    System.out.println(valide);
                    grapheSD=chemin;
                    if(ecart!=1000 && valide){
                        i=0;
                        do{//sens inverse
                            sommets=sauvsom.copie();
                            genGraph(D);
                            chemin=PCC(A, D);
                            i++;
                            System.out.println(i);
                            }while(chemin.trouve(new Sommet(D))<0&&i<10);
                        System.out.println("SI 2 "+i);
                        if(i>=10||chemin.trouve(new Sommet(D))<0){
                            valide=false;
                            sauvsom=somtemp;
                        }
                        grapheSI=chemin;
                    }
                }while(!valide);
            }
            if(ecart==1000){
                w=grapheSD.get(grapheSD.size()-1);
                while(!w.p.equals(D)){
                    cheminf.add(w);
                    w=w.getPred();
                }
                cheminf.add(grapheSD.get(0));
            }
            else if(ecart==1010){
                cheminf=petitPoucet(grapheSD, grapheSI, ecart);
            }
            sauvsom=cheminf;
            /*System.out.println(cheminf.trouve(new Sommet(D)));
            System.out.println(cheminf.trouve(new Sommet(A)));*/
            }
   
    public Liste PCC(Point source, Point arrivee){
        Liste graphe=new Liste();
        Sommet w;
        initDijkstra(source);
        sommets.inverser(1, sommets.trouve(new Sommet(source)));
        sommets.add(sommets.get(0));
        sommets.set(0, null);
        while(sommets.size()>1){
            w=sommets.get(1);
            sommets.supprimer(w);
            if(w.getDis()<Math.pow(2, 31))
                graphe.add(w);
            majDistances(w);
        }
        
        return graphe;
    }
    public Liste petitPoucet(Liste grapheSD, Liste grapheSI, int ecart){
        Liste chemin=new Liste();
        int i=0;
        for(Sommet som: grapheSD.contenu){
            i=grapheSI.trouve(som);
            if(i>=0 && ((som.getDis()+grapheSI.get(i).getDis())/grapheSD.get(grapheSD.size()-1).getDis()<(ecart/1000.0))){
                chemin.add(som);
            }
        }
        return chemin;
    }
    
    private void majDistances(Sommet s){
        for(Sommet v : s.voisins.contenu){
            if((sommets.indexOf(v)>=0)&&(s.getDis()+s.p.distance(v.p)<v.getDis())){
                v.setDis(s.getDis()+s.p.distance(v.p));
                v.setPred(s);
                sommets.retri(v);
                }
            }
        }
    private void initDijkstra(Point source){
        for(Sommet s: sommets.contenu){
            if(s.p.equals(source)){
                s.setDis(0);
                s.setPred(s);
            }
            else{
                s.setDis(Math.pow(2, 31));
                s.setPred(null);
            }
        }
    }
    public void genGraph(Point arrivee){
        if(arrivee.equals(new Point(800, 0))){//sens direct
            for(Sommet s1: sommets.contenu){
                for(Sommet s2: sommets.contenu){
                    if (s1!=s2 && (s2.p.estPlusGrandQue(s1.p)) && s2.p.distance(s1.p)<raygraph){
                        s1.setVoisin(s2);
                    }
                }
            }
        }
        else{//sens inverse
            for(int i=0; i<sommets.size(); i++){
                for(int j=0; j<sommets.size(); j++){
                    if (i!=j && (!sommets.get(j).p.estPlusGrandQue(sommets.get(i).p)) && sommets.get(j).p.distance(sommets.get(i).p)<raygraph){
                        sommets.get(i).setVoisin(sommets.get(j));
                    }
                }
            }
        }
    }
    public void creerObstacles(){
        obstacles[0]=new Cercle(new Point(550, 100), 100);
        obstacles[1]=new Cercle(new Point(400, 350), 100);
        obstacles[2]=new Rectangle(new Point(50, 200), 150, 200);
        obstacles[3]=new Rectangle(new Point(450, 500), 150, 150);
        /*
	int choix, v1, v2, v3, v4;
            Obstacle obs;
            for(int i=0; i<obstacles.length; i++){
                choix=(int)(Math.random()*10)%2;
                if(choix==0){
                    v1=(int)(Math.random()*(taille[1]/4)+1);
                    do{
                        v2=(int)(Math.random()*taille[0]+1);
                        v3=(int)(Math.random()*taille[1]+1);
                    }while(v2<v1||v3<v1||v2>taille[0]-v1||v2>taille[1]-v1);
                    obs=new Cercle(new Point(v2, v3), v1);
                }
                else{
                    v1=(int)(Math.random()*(taille[0])+1);
                    v2=(int)(Math.random()*(taille[1])+1);
                    do{
                        v3=(int)(Math.random()*(taille[0]/4)+1);
                        v4=(int)(Math.random()*(taille[1]/4)+1);
                    }while(v3+v1>taille[0]||v4+v2>taille[1]);
                    obs=new Rectangle(new Point(v1, v2), v3, v4);
                }
                obstacles[i]=obs;	
            }*/
    }
    public void genSommets(){
            int v1, v2, b;
            Point p;
            boolean valide;
            sauvsom.clear();
            sauvsom.add(new Sommet(new Point(taille[0], 0)));
            sauvsom.add(new Sommet(new Point(0, taille[1])));
            for (int i=1; i<nbpts-1; i++){
                do{
                    valide=true;
                    v1=(int)(Math.random()*taille[0]+1);
                    v2=(int)(Math.random()*taille[1]+1);
                    p=new Point(v1, v2);
                    b=0;
                    if(sauvsom.trouve(new Sommet(p))>=0){//evite les doublons
                            valide=false;
                        }
                    do{//verifie que le point ne soit pas sur l'obstacle
                        if(obstacles[b].collision(p))
                            valide=false;
                        b++;
                    }while(valide&&b<obstacles.length);
                }while(!valide);
                sauvsom.add(new Sommet(p));
            }
            sommets=sauvsom.copie();
        }
    public void genSommets(Liste chemin){
        int v1, v2, b;
        Point p;
        boolean valide;
        sommets=chemin.copie();
            for(Sommet s: chemin.contenu){
                for(int i=0; i<5; i++){
                    do{
                        valide=true;
                        v1=(int)(Math.random()*rayraff+1)-(rayraff/2);//genere des entiers dans [ -rayraff/2 ; ... ; +rayraff/2 ]
                        v2=(int)(Math.random()*rayraff+1)-(rayraff/2);
                        p=new Point(s.getPoint().getX()+v1, s.getPoint().getY()+v2);
                        b=0;
                        if(v2>=600 ||v2<=0 || v1>=800 || v1<=0){//evite de construire des points en dehors du cadre
                            valide=false;
                        }
                        do{//verifie que le point ne soit pas sur l'obstacle
                            if(obstacles[b].collision(p))
                                valide=false;
                            b++;
                        }while(valide&&b<obstacles.length);
                    }while(!valide);
                    if(sommets.trouve(new Sommet(p))<0)
                        sommets.add(new Sommet(p));
                }
            }
        sauvsom=sommets.copie();
        }
    public int[] getTaille() {
		return taille;
    }
    //Fonctions de dessin

public void dessineObstacles(Graphics g)
    {
        g.setColor(Color.RED);
            for (Obstacle obstacle : obstacles) {
                if (obstacle instanceof Cercle) {
                    Cercle cerc = (Cercle) obstacle;
                    g.fillOval(cerc.getCentre().getX()+shift-(int)cerc.getRayon(), cerc.getCentre().getY()+shift-(int)cerc.getRayon(), (int)cerc.getRayon()*2, (int)cerc.getRayon()*2);
                } else {
                    Rectangle rect = (Rectangle) obstacle;
                    g.fillRect(rect.getP().getX()+shift, rect.getP().getY()+shift, rect.getLarg(), rect.getHaut());
                }
            }
    }
    public void dessineSommets(Graphics g)
    {
        g.setColor(Color.BLACK);
        for(int j=1; j<sommetsaafficher.size(); j++)
        {
            g.fillOval(sommetsaafficher.get(j).getPoint().getX()+shift-(pointsize/2), sommetsaafficher.get(j).getPoint().getY()+shift-(pointsize/2), pointsize, pointsize);
            if(sommetsaafficher.get(j).voisins!=null){
                for(Sommet som: sommetsaafficher.get(j).voisins.contenu){
                    g.drawLine(sommetsaafficher.get(j).getPoint().getX()+shift+(pointsize/2), sommetsaafficher.get(j).getPoint().getY()+shift+(pointsize/2), som.getPoint().getX()+shift+(pointsize/2), som.getPoint().getY()+shift+(pointsize/2));
                }
            }
        }
    }
    public void dessinechemin(Graphics g, int stade){
        g.setColor(Color.blue);
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(3));
        for(int i=0; i<cheminf.size()-1; i++){
            g.fillOval(cheminf.get(i).getPoint().getX()+shift-(pointsize/2), cheminf.get(i).getPoint().getY()+shift-(pointsize/2), pointsize*2, pointsize*2);
            if(stade==1000)
                g.drawLine(cheminf.get(i).getPoint().getX()+shift+(pointsize/2), cheminf.get(i).getPoint().getY()+shift+(pointsize/2), cheminf.get(i+1).getPoint().getX()+shift+(pointsize/2), cheminf.get(i+1).getPoint().getY()+shift+(pointsize/2));
        }
        g.fillOval(cheminf.get(cheminf.size()-1).getPoint().getX()+shift-(pointsize/2), cheminf.get(cheminf.size()-1).getPoint().getY()+shift-(pointsize/2), pointsize*2, pointsize*2);
    }
    
}
