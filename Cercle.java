/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijkstra;
/**
 *
 * @author im285503
 */
public class Cercle extends Obstacle{
	Point centre;
	double rayon;

	public Cercle(Point centre, int rayon) {
		this.centre = centre;
		this.rayon = rayon;
	}
	public Point getCentre() {
		return centre;
	}
	public double getRayon() {
		return rayon;
	}
	public void setCentre(Point centre) {
		this.centre = centre;
	}
	public void setRayon(double rayon) {
		this.rayon = rayon;
	}
        @Override
	public boolean collision(Point p){ //renvoie true si le point est dans l'obstacle
            return (this.getCentre().distance(p)<this.getRayon());    
        }

    @Override
    public String toString() {
        return "C{" + "c=" + centre + ", r=" + rayon + '}';
    }
        
}
